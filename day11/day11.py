octopuses = []
# Read input
with open("input.txt") as f:
    for line in f:
        octopuses.append(line.strip())

# Split into individual numbers
octopuses = list(map(lambda o: [int(a) for a in str(o)], octopuses))

# Helper functions
def print_area(area):
    for row in area:
        #print(row)
        print("".join(str(i) for i in row))
    print("\n")

# Part 1
num_steps = 195
flash_count = 0
i = 0
new_levels = []
in_sync = False

# Helper for flashes
def try_flash(x, y):
    global new_levels
    global flash_count
    # End handling if out of bounds
    if y < 0 or y >= len(new_levels) or x < 0 or x >= len(new_levels[y]):
        return
    # Increase energy levels
    new_levels[y][x] += 1
    # Flash when energy level hits 10
    if new_levels[y][x] == 10:
        flash_count += 1
        # Try to flash each area around current location
        a = x
        b = y
        for b in range(y - 1, y + 2):
            for a in range(x - 1, x + 2):
                try_flash(a, b)

# Count number of flashes in given steps
while not in_sync:
    # Step number
    i += 1
    # Initialize new energy levels
    new_levels = octopuses
    # Initialize coordinates
    x = 0
    y = 0
    # Loop through all octopuses
    for y in range(0, len(octopuses)):
        for x in range(0, len(octopuses[y])):
            try_flash(x, y)
    # change all values greater than 9 into 0
    x = 0
    y = 0
    for y in range(0, len(new_levels)):
        for x in range(0, len(new_levels[y])):
            if new_levels[y][x] > 9:
                new_levels[y][x] = 0
    # Set new state
    octopuses = new_levels

    # Report part 1 findings
    if i == num_steps:
        # Report results
        print(f"==== PART 1 ====")
        print(f"Flash count: {flash_count}\n")
    
    # Check if in sync
    sum_energy_levels = 0
    for row in new_levels:
        sum_energy_levels += sum(row)
    in_sync = sum_energy_levels == 0

# Report results
print(f"==== PART 2 ====")
print(f"First time when all octopuses are in sync: step {i}\n")