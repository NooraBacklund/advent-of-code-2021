import heapq
import sys

# Read in data
risk_levels = [list(map(int, row)) for row in open("input.txt", "r").read().split('\n')]

# Set up variables
width, height = len(risk_levels[0]), len(risk_levels)
total_risk = {(i, j): sys.maxsize for i in range(width) for j in range(height)}
nodes_to_visit = [(0, (0, 0))]

# Traverse nodes
while(nodes_to_visit):
    current_risk, current_node = heapq.heappop(nodes_to_visit)
    # Get neighboring nodes
    curr_x, curr_y = current_node
    neighbors = [(curr_x - 1, curr_y), (curr_x + 1, curr_y), (curr_x, curr_y - 1), (curr_x, curr_y + 1)]
    # Calculate risk in existing neighbors:
    for neighbor in list(filter(lambda c: c in total_risk, neighbors)):
        new_neighbor_risk = current_risk + risk_levels[neighbor[1]][neighbor[0]]
        if total_risk[neighbor] > new_neighbor_risk:
            total_risk[neighbor] = new_neighbor_risk
            heapq.heappush(nodes_to_visit, (total_risk[neighbor], neighbor))

print(f"==== PART 1 ====")
print(f"Total risk of least risk path: {total_risk[width - 1, height - 1]}\n")

# Part 2
# Re-initialize total risks, this time for a larger area
total_risk = {(i, j): sys.maxsize for i in range(width * 5) for j in range(height * 5)}
nodes_to_visit = [(0, (0, 0))]

# Traverse nodes (again)
while (nodes_to_visit):
    current_risk, current_node = heapq.heappop(nodes_to_visit)
    # Get neighboring nodes
    curr_x, curr_y = current_node
    neighbors = [(curr_x - 1, curr_y), (curr_x + 1, curr_y), (curr_x, curr_y - 1), (curr_x, curr_y + 1)]
    # Calculate risk in existing neighbors
    for neighbor in list(filter(lambda c: c in total_risk, neighbors)):
        x_tile_offset, y_tile_offset = neighbor[0] // width, neighbor[1] // height
        x, y = neighbor[0], neighbor[1]
        neighbor_base_risk = (risk_levels[x % width][y % height] + x_tile_offset + y_tile_offset) % 9
        if neighbor_base_risk == 0:
            neighbor_base_risk = 9
        new_neighbor_risk = current_risk + neighbor_base_risk
        if total_risk[neighbor] > new_neighbor_risk:
            total_risk[neighbor] = new_neighbor_risk
            heapq.heappush(nodes_to_visit, (new_neighbor_risk, neighbor))

print(f"==== PART 2 ====")
print(f"Total risk of least risk path: {total_risk[width * 5 - 1, height * 5 - 1]}\n")