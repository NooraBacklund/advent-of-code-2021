#   0:      1:      2:      3:      4:
#  aaaa    ....    aaaa    aaaa    ....
# b    c  .    c  .    c  .    c  b    c
# b    c  .    c  .    c  .    c  b    c
#  ....    ....    dddd    dddd    dddd
# e    f  .    f  e    .  .    f  .    f
# e    f  .    f  e    .  .    f  .    f
#  gggg    ....    gggg    gggg    ....
# 
#   5:      6:      7:      8:      9:
#  aaaa    aaaa    aaaa    aaaa    aaaa
# b    .  b    .  .    c  b    c  b    c
# b    .  b    .  .    c  b    c  b    c
#  dddd    dddd    ....    dddd    dddd
# .    f  e    f  .    f  e    f  .    f
# .    f  e    f  .    f  e    f  .    f
#  gggg    gggg    ....    gggg    gggg
#

# Read in data
with open("input.txt") as f:
    data = f.readlines()

# Variables
part1_count = 0
part2_count = 0

# Translate each line in data
for line in data:
    # Translate line into data
    line = line.split('|')
    patterns = line[0].strip().split(' ')
    output = line[1].strip().split(' ')
    # Initialize variables
    a, b, c, d, e, f, g = '?', '?', '?', '?', '?', '?', '?'
    n1, n2, n3, n4, n5, n6, n7, n8, n9, n0 = '?', '?', '?', '?', '?', '?', '?', '?', '?', '?'
    # Determine numbers
    # 1 is the only item to have two letters
    n1 = "".join(filter(lambda p: len(p) == 2, patterns))
    patterns.remove(n1)
    # 7 is the only item to have three letters
    n7 = "".join(filter(lambda p: len(p) == 3, patterns))
    patterns.remove(n7)
    # 8 is the only item to have all signals active
    n8 = "".join(filter(lambda p: len(p) == 7, patterns))
    patterns.remove(n8)
    # Determine signal position for a (only letter that is in 7 but not in 1)
    a = "".join(filter(lambda x: not x in n1, n7))
    # 4 is only item with four letters
    n4 = "".join(filter(lambda p: len(p) == 4, patterns))
    patterns.remove(n4)
    # Segment c is missing from number 6 (either segment present in number 1), length 6
    n6 = "".join(filter(lambda p: len(p) == 6 and not(n1[0] in p and n1[1] in p), patterns))
    patterns.remove(n6)
    # Determine segemnt c & f
    if n1[0] not in n6:
        c = n1[0]
        f = n1[1]
    else:
        c = n1[1]
        f = n1[0]
    # 5 is the remaining combination with no segment c
    n5 = "".join(filter(lambda p: not c in p, patterns))
    patterns.remove(n5)
    # Segment e is not present in 5 but is present in 6
    e = "".join(filter(lambda x: not x in n5, n6))
    # 9 contains same segments as 5, plus segment c
    n9 = "".join(filter(lambda p: sorted(p) == sorted(n5 + c), patterns))
    patterns.remove(n9)
    # 0 is the only remaining pattern with six letters
    n0 = "".join(filter(lambda p: len(p) == 6, patterns))
    patterns.remove(n0)
    # Segment d is in 8 but not in 0
    d = "".join(filter(lambda x: not x in n0, n8))
    # Segment e is not present in number 3
    n3 = "".join(filter(lambda p: not e in p, patterns))
    patterns.remove(n3)
    # Last one is number 2
    n2 = patterns[0]
    # segment b is in 8, but not 3 + segment e
    b = "".join(filter(lambda x: not x in (n3 + e), n8))
    # Segment g is in 3, but not 7 + segment d
    g = "".join(filter(lambda x: not x in (n7 + d), n3))

    # Print output
    numbers = [n0, n1, n2, n3, n4, n5, n6, n7, n8, n9]
    numbers = list(map(lambda n: sorted(n), numbers))
    code = ""
    for number in output:
        idx = numbers.index(sorted(number))
        code += str(idx)
        # Part 1 answer creation
        if idx in (1, 4, 7, 8):
            part1_count += 1
    # Part 2 answer creation
    part2_count += int(code)

print(f"==== PART 1 ====")
print(f"Count of 1, 4, 7, 8: {part1_count}\n")

print(f"==== PART 2 ====")
print(f"Sum of all codes: {part2_count}\n")