# Initialize variables
pt1_vent_locations = {}
pt2_vent_locations = {}
pt1_vent_count = 0
pt2_vent_count = 0

# Functions
def add_vent(x, y, mode):
    coordinate_found = False
    global pt2_vent_count
    global pt1_vent_count
    if mode == 'part1':
        vent_list = pt1_vent_locations
        vent_count = pt1_vent_count
    else:
        vent_list = pt2_vent_locations
        vent_count = pt2_vent_count
    # Check if coordinate already exists in vent locations
    if x in vent_list:
        if y in vent_list[x]:
            # Increase count
            vent_list[x][y] += 1
            coordinate_found = True
            # If count == 2 (new overlap), this needs to be added to vent counts
            if vent_list[x][y] == 2:
                vent_count += 1
    # Add new vent location
    if not coordinate_found:
        if x in vent_list:
            vent_list[x][y] = 1
        else:
            vent_list[x] = {}
            vent_list[x][y] = 1
    # Update vent count
    if mode == 'part1':
        pt1_vent_count = vent_count
    else:
        pt2_vent_count = vent_count


# Read in data
with open("input.txt") as f:
    for line in f:
        # Parse each line and turn into coordinates
        start = list(map(lambda x: int(x), line.split(' -> ')[0].split(',')))
        end = list(map(lambda y: int(y), line.split(' -> ')[1].rstrip().split(',')))
        
        # PART 1: only handle horizontal lines
        if start[0] == end[0]:
            # Add vent locations to list (both parts 1 and 2)
            for y in range(min(start[1], end[1]), max(start[1], end[1]) + 1):
                add_vent(start[0], y, 'part1')
                add_vent(start[0], y, 'part2')
        # PART 1: ....and vertical lines
        elif start[1] == end[1]:
            # add vent locations to lists (both parts 1 and 2)
            for x in range(min(start[0], end[0]), max(start[0], end[0]) + 1):
                add_vent(x, start[1], 'part1')
                add_vent(x, start[1], 'part2')
        # PART 2: diagonals
        else:
            # i is used to create diagonal y value
            i = 0
            # used to determine if y-value should increase or decrease
            direction = 1
            if start[0] < end[0]:
                if start[1] > end[1]:
                    direction = -1
            else:
                if start[1] < end[1]:
                    direction = -1
            for x in range(min(start[0], end[0]), max(start[0], end[0]) + 1):
                # Get correct starting y value
                if start[0] > end[0]:
                    y = end[1]
                else:
                    y = start[1]
                # Add vent
                add_vent(x, y + i, 'part2')
                # Increase y value offset
                i += 1 * direction

# Report results
print(f"==== PART 1 ====")
print(f"Overlapping vent count: {pt1_vent_count}\n")
print(f"==== PART 2 ====")
print(f"Overlapping vent count: {pt2_vent_count}\n")