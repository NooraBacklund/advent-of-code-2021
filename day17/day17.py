import re
import math

# Define target area
#target = "target area: x=20..30, y=-10..-5" # Test input
target = "target area: x=138..184, y=-125..-71" # Actual input

# Grab coordinates from input
(t_x_min, t_x_max, t_y_min, t_y_max) = re.findall("[-\d]+", target)

# Generate list of possible x-values
x_values = []
x_values_are_positive = True
if t_x_min[0] == "-":
    t_x_max, t_x_min = t_x_min[1:], t_x_max[1:]
    x_values_are_positive = False
for k in range(int(t_x_max), 0, -1):
    # Find x-values which hit the target area
    v_x = k
    curr_x = 0
    while True:
        curr_x += v_x
        if curr_x > int(t_x_max): break
        if curr_x >= int(t_x_min) and not k in x_values:
            x_values.append(k)
        v_x -= 1
        if v_x == 0: break

# Find correct y-values which hit the target area
y_max = 0
possible_velocities = []
for x in x_values:
    # Range(a, b):
    # a = minimum y-velocity needed to hit target area
    # b = trajectory is symmetrical and always hits y = 0, stop after last time that y will hit target area 
    for y in range(int(t_y_min), abs(int(t_y_min))):
        curr_max_y = 0
        curr_y = 0
        curr_x = 0
        v_x = x
        k = 0
        while True:
            # Calculate next y-value
            curr_y += y - k
            curr_x += v_x
            # Update max y-value if needed
            if curr_y > curr_max_y:
                curr_max_y = curr_y
            # End after target area has passed
            if curr_y < int(t_y_min) or curr_x > int(t_x_max):
                break
            # Check if target was hit
            if curr_y >= int(t_y_min) and curr_y <= int(t_y_max) and curr_x >= int(t_x_min) and curr_x <= int(t_x_max):
                # Update global max y if needed
                if curr_max_y > y_max:
                    y_max = curr_max_y
                # Add velocity to list
                if (x, y) not in possible_velocities:
                    possible_velocities.append((x, y))
            k += 1
            if v_x > 0:
                v_x -= 1

# Report results
print(f"==== PART 1 ====")
print(f"Maximum y-value: {y_max}\n")

print(f"==== PART 2 ====")
print(f"Total velocities: {len(possible_velocities)}\n")