def get_fish_count(fish_list, days_left):
    fish_birthdays = {}
    # Parse initial fish list into days when new fish will emerge
    for fish in fish_list:
        day = fish + 1
        # Mark down first birthday
        if day in fish_birthdays:
            fish_birthdays[day] += 1
        else:
            fish_birthdays[day] = 1
        day += 7
        while day <= days_left:
            if (day) in fish_birthdays:
                fish_birthdays[day] += 1
            else:
                fish_birthdays[day] = 1
            day += 7
    
    # Iterate through the list to create new birthdays for all new fish born
    for i in range (1, days_left):
        # If new fish were born that day, add birthdays for their offspring
        if i in fish_birthdays:
            # Current day
            day = i
            # Number of fish that were born at the same time
            n = fish_birthdays[i]
            # First ones will be born in 9 days (timer 8 -> 0)
            day += 9
            if day <= days_left:
                if day in fish_birthdays:
                    fish_birthdays[day] += n
                else:
                    fish_birthdays[day] = n
            # Calculate all remaining new fish (every 7 days):
            day += 7
            while day <= days_left:
                if day in fish_birthdays:
                    fish_birthdays[day] += n
                else:
                    fish_birthdays[day] = n
                day += 7
    
    # Sum up number of fish born on each day
    num_fish = len(fish_list)
    for day in fish_birthdays:
        num_fish += fish_birthdays[day]
    return num_fish

# Read in data
with open("input.txt") as f:
    age_list = f.readline().rstrip().split(',')

# Transform ages into numbers
age_list = list(map(lambda a: int(a), age_list))

# Set number of days to simulate for
num_days = 256

# Report results
print(f"Number of fish after {num_days} days: {get_fish_count(age_list, num_days)}")