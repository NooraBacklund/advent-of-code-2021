# Read input
with open("input.txt") as f:
    data = f.readlines()

# Turn into dictionary of paths
data = [x.strip() for x in data]
forward = [(x.split('-')[0], x.split('-')[1]) for x in data]
reverse = [(x.split('-')[1], x.split('-')[0]) for x in data]
paths = forward + reverse

# Initialize variables
current_loc = 'start'
target_loc = 'end'
possible_paths = []
dead_ends = []

possible_paths_pt2 = []
dead_ends_pt2 = []

# Helper functions
def move(curr, past):
    global target_loc
    global possible_paths
    global paths
    visited = list(past)
    # Add current location to visited list
    visited.append(curr)
    # Return current location when at target location (= done!)
    if curr == target_loc:
        possible_paths.append(visited)
        return True
    # Get possible locations to move to
    next_locs = list(filter(lambda x: x[0] == curr, paths))
    # Filter out visited locations which can't be visited again
    next_locs = list(filter(lambda x: x[1].isupper() or not x[1] in visited, next_locs))
    # If no locations are left, add to dead ends and end handling
    if len(next_locs) == 0:
        dead_ends.append(visited)
        return False
    # Move to next possible locations
    for loc in next_locs:
        move(loc[1], visited)

def move_pt2(curr, past, can_visit_twice):
    global target_loc
    global possible_paths_pt2
    global paths
    visited = list(past)
    double_visit_ok = can_visit_twice
    # Add current location to visited list
    visited.append(curr)
    # If double visit to small cave is allowed, check if this is it
    if double_visit_ok:
        # Set to false if this is the second visit to a small cave
        if not curr.isupper() and not (curr == 'end' or curr == 'start') and visited.count(curr) == 2:
            double_visit_ok = False
    # Return current location when at target location (= done!)
    if curr == target_loc:
        possible_paths_pt2.append(visited)
        return True
    # Get possible locations to move to
    next_locs = list(filter(lambda x: x[0] == curr, paths))
    # Filter out visited locations which can't be visited again
    next_locs = list(filter(lambda x: x[1] != 'start' and \
                            (x[1].isupper() or \
                            not x[1] in visited or \
                            (visited.count(x[1]) < 2 and double_visit_ok)), next_locs))
    # If no locations are left, add to dead ends and end handling
    if len(next_locs) == 0:
        dead_ends_pt2.append(visited)
        return False
    # Move to next possible locations
    for loc in next_locs:
        move_pt2(loc[1], visited, double_visit_ok)

# Find next possible steps from current location
move(current_loc, [])
move_pt2(current_loc, [], True)

# Report findings
print(f"==== PART 1 ====")
print(f"Number of paths to {target_loc}: {len(possible_paths)}\n")

print(f"==== PART 2 ====")
print(f"Number of paths to {target_loc}: {len(possible_paths_pt2)}\n")
