# Variable declarations
coordinates = []
fold_instructions = []
width = 0
height = 0

# Read in data
with open("input.txt") as f:
    # Read in point locations
    line = f.readline().strip()
    while line != "":
        # Parse into coordinate
        l = line.split(",")
        x = int(l[0])
        y = int(l[1])
        # Update width higher than current
        if x > width: width = x
        if y > height: height = y
        coordinates.append((x, y))
        line = f.readline().strip()
    
    # Read in and parse directions
    line = f.readline().strip()
    while line != "":
        l = line.split("=")
        fold_instructions.append((l[0][-1], int(l[1])))
        line = f.readline().strip()

# Helper functions
def fold(dir, loc):
    global coordinates
    global width
    global height
    # Determine direction
    if dir == 'y':
        # Need to alter y-coordinates
        # Shift all coordinates up halfway and take abs value to mimic fold
        coordinates = list(map(lambda c: (c[0], abs(c[1] - loc) - 1), coordinates))
        # Set new height
        height = max(loc - 1, height - loc - 1)
    elif dir == 'x':
        # Need to alter x-coordinates
        # Shift all coordinates left halfway and take abs value to mimic fold
        coordinates = list(map(lambda c: (abs(c[0] - loc) - 1, c[1]), coordinates))
        # Set new width
        width = max(loc - 1, width - loc - 1)
    # Remove duplicates
    coordinates = list(set(coordinates))

def print_coordinates(area):
    # Get max and min for output
    x_min = min([c[0] for c in area])
    x_max = max([c[0] for c in area])
    y_min = min([c[1] for c in area])
    y_max = max([c[1] for c in area])
    print()
    for y in range(y_min, y_max + 1):
        str = ""
        for x in range(x_min, x_max + 1):
            if (x, y) in area:
                str += "#"
            else:
                str += " "
        print(str)
    print()

# Part 1 & 2: fold all instructions
first_part = True
for instruction in fold_instructions:
    fold(instruction[0], instruction[1])
    # First part only wants the dot count after first fold
    if first_part:
        first_part = False
        print(f"==== PART 1 ====")
        print(f"Coordinate points on map: {len(coordinates)}\n")
# Report part 2 results:
print(f"==== PART 2 ====")
# Flip y and x axes to produce readable stuff
coordinates = list(map(lambda c: (-c[0], -c[1]), coordinates))
print_coordinates(coordinates)