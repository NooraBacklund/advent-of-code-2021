# Read in data
with open("input.txt") as f:
    data = f.readlines()

# Remove control characters
data = list(map(lambda l: l.strip(), data))

# Find risk levels
risk_levels = []
x = 0
y = 0
for y in range(0, len(data)):
    for x in range(0, len(data[y])):
        curr_value = data[y][x]
        # Check left
        if x > 0:
            if data[y][x - 1] <= curr_value: continue
        # Check right
        if x < len(data[y]) - 1:
            if data[y][x + 1] <= curr_value: continue
        # Check up
        if y > 0:
            if data[y - 1][x] <= curr_value: continue
        # Check right
        if y < len(data) - 1:
            if data[y + 1][x] <= curr_value: continue
        # No lower values found - log current value as low point
        risk_levels.append(int(curr_value) + 1)

# Report results
print(f"==== PART 1 ====")
print(f"Risk level: {sum(risk_levels)}\n")

# Part 2: Calculating basin sizes

# Move row by row from left to right
# For each non-nine number found, locate all adjacent basin locations (and recurse)
# Also turn the visited locations into 9's (to prevent duplicates)

# Variables
area = data
area = list(map(lambda r: list(r), area))
basins = []

# Helper functions
def get_basin_size(row, column):
    # Check if out of bounds (return 0)
    if row < 0 or row > len(area) - 1 or column < 0 or column > len(area[row]) - 1:
        return 0
    # If location is not part of basin, return 0
    if area[row][column] == '9':
        return 0
    # Otherwise:
    # Mark location as visited
    area[row][column] = '9'
    # Return size plus size of all children
    return 1 + get_basin_size(row, column - 1) + get_basin_size(row, column + 1) + get_basin_size(row - 1, column) + get_basin_size(row + 1, column)

def print_area(map):
    for a in map:
        print("".join(a))

# Calculate basin sizes
x = 0
y = 0
for y in range(0, len(area)):
    for x in range(0, len(area[y])):
        # Check if unvisited basin was found
        if area[y][x] != '9':
            basins.append(get_basin_size(y, x))
            #print_area(area)
            #print("--------")

# Report results
basins.sort(reverse=True)
print(f"==== PART 2 ====")
print(f"Three largest basins: {basins[:3]}")
print(f"Multiplied: {basins[0] * basins [1] * basins[2]}\n")

