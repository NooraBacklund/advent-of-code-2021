from bingocard import BingoCard

with open("input.txt") as f:
    lines = f.readlines()

# Get the bingo numbers, remove numbers (and empty line) from list
bingo_numbers = lines[0].split(',')
lines.pop(0)
lines.pop(0)

# Create bingo cards
bingo_cards = []
card_rows = []
for row in lines:
    if row.rstrip() != '':
        # Add non-empty rows to card_rows for bingo card construction
        card_rows.append(row.rstrip().split())
    else: 
        # Create a card from rows
        bingo_cards.append(BingoCard(card_rows))
        # Empty list of rows for next card
        card_rows = []
# Create last card if not already created
if len(card_rows) > 0:
    bingo_cards.append(BingoCard(card_rows))

# Play bingo (part 1: try to win)
for number in bingo_numbers:
    # Check each card in turn
    bingo = False
    for card in bingo_cards:
        bingo = card.mark_number(number) and True #This sets bingo as True only if card calls bingo
        # If bingo was called, report score and end
        if bingo:
            print(f"==== PART 1 ====")
            print(f"Bingo was called!")
            print(f"The last number was {number}")
            print(f"Score of calling card is {card.score}")
            print(f"Final score is {int(number) * card.score}\n")
            break
    if bingo: break

# Play bingo (part 2: let the squid win)
last_board_in_play = False
for number in bingo_numbers:
    # Check each card in turn
    for card in bingo_cards:
        card.mark_number(number)
        # If last board calls bingo, report scores
        if last_board_in_play and card.bingo:
            print(f"==== PART 2 ====")
            print(f"Found the last board to win")
            print(f"Last board wins when number {number} is called")
            print(f"The score of last card is {bingo_cards[0].score}")
            print(f"Final score is {int(number) * bingo_cards[0].score}")
            break
    # Remove all bingo'ed cards from play
    bingo_cards = list(filter(lambda card: not card.bingo, bingo_cards))
    # Report results when only one bingo card is left in play
    if len(bingo_cards) == 1:
        last_board_in_play = True
    # End game when no more bingo cards are in play
    elif len(bingo_cards) == 0:
        break
        
        
