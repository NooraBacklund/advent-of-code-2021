class BingoCard:
    # Expects lines to be in five arrays of five item arrays
    def __init__(self, lines):
        self.bingo = False
        self.rows = lines
        self.score = 0
        self.columns = [[], [], [], [], []]
        for row in lines:
            i = 0
            for number in row:
                self.columns[i].append(number)
                i += 1
    
    def mark_number(self, num):
        # Check rows first
        for row in self.rows:
            # Remove entry from list if found
            if num in row:
                row.remove(num)
            # Return True if bingo (row empty)
            if len(row) == 0:
                self.__calculate_score()
                self.bingo = True
                return True
        # Check columns next
        for column in self.columns:
            # Remove entry from list if found
            if num in column:
                column.remove(num)
            # Return True if bingo (column empty)
            if len(column) == 0:
                self.__calculate_score()
                self.bingo = True
                return True
        # No bingo calls --> return False
        return False

    def __calculate_score(self):
        self.score = 0
        for row in self.rows:
            if len(row) > 0:
                self.score += sum(map(lambda x: int(x), row))