# Read input
with open("input.txt") as f:
    # Get the template
    template = f.readline().strip()

    # Read away the empty line
    f.readline()
    # Read in substitution rules
    rules = {}
    rule = f.readline().strip()
    while rule != "":
        # Parse rule
        # Result: ((first_char, second_char), insertion)
        rule = rule.split(" -> ")
        rules[(rule[0][0], rule[0][1])] = rule[1]
        rule = f.readline().strip()

# Keep track of letter counts
character_counts = {}

def add_character(c, n):
    global character_counts
    if c in character_counts:
        character_counts[c] += n
    else:
        character_counts[c] = n

# Perform insertion
num_steps = 40
step = 1
character_pairs = {}
last_character = template[-1]

# Create character pairs
def add_character_pair(pair, count, dict):
    if pair in dict:
        dict[pair] += count
    else:
        dict[pair] = count

for x in zip(template, template[1:]):
    add_character_pair(x, 1, character_pairs)

# Create new character pairs for each step
for step in range(1, num_steps + 1):
    # Turn each character pair into new set of character pairs
    new_pairs = {}
    for pair in character_pairs:
        # Check if insertion needs to happen
        if pair in rules.keys():
            # Get character to be inserted
            char = rules[pair]
            # Get number of similar character pairs
            num_pairs = character_pairs[pair]
            # Add new pairs
            add_character_pair((pair[0], char), num_pairs, new_pairs)
            add_character_pair((char, pair[1]), num_pairs, new_pairs)
    character_pairs = new_pairs

# Count characters
for pair in character_pairs:
    add_character(pair[0], character_pairs[pair])
# Add last character
add_character(last_character, 1)

# Part 1: Show difference between most common and least common characters
pt1_min = character_counts[min(character_counts.keys(), key=(lambda c: character_counts[c]))]
pt1_max = character_counts[max(character_counts.keys(), key=(lambda c: character_counts[c]))]
print(f"==== PART 1 / 2 ====")
print(f"Steps: {num_steps}")
print(f"Difference between highest and lowest counts: {pt1_max - pt1_min}\n")