# Helper functions
def move(loc, dir, dist):
    new_loc = loc
    if dir == 'forward':
        # part 1
        new_loc[0][0] += dist
        # part 2
        new_loc[1][0] += dist
        new_loc[1][1] += new_loc[2] * dist
    elif dir == 'down':
        # part 1
        new_loc[0][1] += dist
        # part 2
        new_loc[2] += dist
    elif dir == 'up':
        # part 1
        new_loc[0][1] -= dist
        # part 2
        new_loc[2] -= dist
    return new_loc

# Read input
with open("input.txt") as f:
    # Initialize location
    # [[<pt1 x>, <pt1 y>], [<pt2 x>, <pt2 y>], <aim (pt2)>]
    location = [[0, 0], [0, 0], 0]

    for command in f:
        # Parse instruction
        direction = command.split(' ')[0]
        distance = int(command.split(' ')[1])
        # Change current location based on instructions
        location = move(location, direction, distance)

print(f"==== PART 1 ====")
print(f"Final location: ({location[0][0]}, {location[0][1]})")
print(f"Answer: {location[0][0] * location[0][1]}\n")

print(f"==== PART 2 ====")
print(f"Final location: ({location[1][0]}, {location[1][1]})")
print(f"Answer: {location[1][0] * location[1][1]}")