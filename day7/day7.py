import statistics
import math

# Read in crab locations
with open("input.txt") as f:
    crabs = f.readline().split(',')
crabs = list(map(lambda c: int(c), crabs))

# Calculate median location for crabs
median = math.floor(statistics.median(crabs))

# Calculate consumed fuel to move each crab to median location
fuel = 0
for crab in crabs:
    fuel += abs(crab - median)

# Report results
print(f"==== PART 1 ====")
print(f"Target location: {median}")
print(f"Fuel required: {fuel}\n")

# Manual stuff. Turn crablist into a crabtionary
crabs.sort()
first_crab = crabs[0]
last_crab = crabs[-1]
crabtionary = {}
for crab in crabs:
    if crab in crabtionary:
        crabtionary[crab] += 1
    else: 
        crabtionary[crab] = 1

# Sum up fuel costs for each crab location
fuel_costs = {}
for i in range(first_crab, last_crab + 1):
    total_cost = 0
    # Sum up cost for each crab:
    for crab in crabtionary:
        # Get distance to move
        distance = abs(i - crab)
        # Calculate cost for each crab
        cost = math.floor((distance * (distance + 1)) / 2)
        # Add to total cost
        total_cost += cost * crabtionary[crab]
    fuel_costs[i] = total_cost

optimal = min(fuel_costs.items(), key=lambda x: x[1])

# Report results
print(f"==== PART 2 ====")
print(f"Optimal location: {optimal[0]}")
print(f"Fuel consumption: {optimal[1]}\n")