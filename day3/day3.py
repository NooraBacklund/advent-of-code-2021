# Helper functions 
def get_oxygen_rating(lines):
    num_lines = len(lines)
    if num_lines == 1:
        return lines[0]
    if num_lines == 0:
        return ''
    # Get most common first bit
    # Get lines with '1' as first bit
    first_bit_is_1 = list(filter(lambda line: line[0] == '1', lines))
    # Check if '1' is most common
    if len(first_bit_is_1) >= num_lines / 2:
        # Recurse into next bits
        first_bit_is_1 = list(map(lambda line: line[1:], first_bit_is_1))
        return '1' + get_oxygen_rating(first_bit_is_1)
    else:
        # Get lines with '0' as first bit
        first_bit_is_0 = list(filter(lambda line: line[0] == '0', lines))
        # Recurse into next bits
        first_bit_is_0 = list(map(lambda line: line[1:], first_bit_is_0))
        return '0' + get_oxygen_rating(first_bit_is_0)

def get_co2_scrubber_rating(lines):
    num_lines = len(lines)
    if num_lines == 1: 
        return lines[0]
    if num_lines == 0:
        return ''
    # Get least common bit first
    # Get lines with '1' as first bit
    first_bit_is_1 = list(filter(lambda line: line[0] == '1', lines))
    # Check if '1' is least common (equal amounts not included in this)
    if len(first_bit_is_1) < num_lines / 2:
        # Recurse into next bits
        first_bit_is_1 = list(map(lambda line: line[1:], first_bit_is_1))
        return '1' + get_co2_scrubber_rating(first_bit_is_1)
    else:
        # Get lines with '0' as first bit
        first_bit_is_0 = list(filter(lambda line: line[0] == '0', lines))
        # Recurse into next bits
        first_bit_is_0 = list(map(lambda line: line[1:], first_bit_is_0))
        return '0' + get_co2_scrubber_rating(first_bit_is_0)


# Read input into list
with open("input.txt") as f:
    lines = f.read().splitlines()

# Sum up ones and zeroes vertically
digit_counts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
line_count = 0
for line in lines:
    line_count += 1
    i = 0
    while i < len(line.rstrip()):
        digit_counts[i] = digit_counts[i] + int(line[i])
        i += 1

# Check which digit (1 or 0) was most common in each position (gamma), also create reverse (epsilon)
gamma_rate = list(map(lambda digit: '1' if (digit >= line_count / 2) else '0', digit_counts))
epsilon_rate = list(map(lambda digit: '1' if (digit < line_count / 2) else '0', digit_counts))

# Part 1:
# Convert into decimal values
gamma_rate = int(''.join(gamma_rate), base=2)
epsilon_rate = int(''.join(epsilon_rate), base=2)

# Calculate power consumption
power_consumption = gamma_rate * epsilon_rate

# Part 2 specific: 
# Locate correct ratings
oxygen_generator_rating = get_oxygen_rating(lines)
co2_scrubber_rating = get_co2_scrubber_rating(lines)

# Convert into decimal values
oxygen_generator_rating = int(oxygen_generator_rating, base=2)
co2_scrubber_rating = int(co2_scrubber_rating, base=2)

# Calculate life support rating
life_support_rating = oxygen_generator_rating * co2_scrubber_rating

# Report results
print(f"==== PART 1 ====")
print(f"Power consumption: {power_consumption}\n")

print(f"==== PART 2 ====")
print(f"Life support rating: {life_support_rating}\n")

