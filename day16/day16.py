# Read input
stream = [line.strip() for line in open("input.txt", "r").readlines()]

# Convert to bits
# Append '1' to start to ensure leading zeroes are preserved and grab the rest
stream = bin(int('1' + stream[0], 16))[3:]

total_versions = 0

def operate_data(data, id):
    if id == 0:
        return sum(data)
    elif id == 1:
        ret = 1
        for d in data:
            ret *= d
        return ret
    elif id == 2:
        return min(data)
    elif id == 3:
        return max(data)
    elif id == 5:
        return int(data[0] > data[1])
    elif id == 6:
        return int(data[0] < data[1])
    elif id == 7:
        return int(data[0] == data[1])


def read_packets(strm):
    global total_versions
    if len(strm) < 8: return
    # Get version and type
    version = int(strm[0:3], 2)
    type = int(strm[3:6], 2)
    strm = strm[6:]
    total_versions += version
    # Type 4 = literal value
    if type == 4:
        # Fetch binary number contained within
        read_on = True
        data = ""
        while read_on:
            read_on = strm[0] == '1'
            data += str(strm[1:5])
            strm = strm[5:]
        return(int(data, 2), strm)
    else: # Operator
        length_type = strm[0]
        packet_data = []
        if length_type == '0':
            sub_packet_length = int(strm[1:16], 2)
            strm = strm[16:]
            sub_packet_data = str(strm[0:sub_packet_length])
            # Collect sub-packet data
            while len(sub_packet_data) > 0:
                data, sub_packet_data = read_packets(sub_packet_data)
                packet_data.append(data)
            return(operate_data(packet_data, type), strm[sub_packet_length:])
        else:
            # Identify number of sub-packets left to read in this packet
            num_sub_packets = int(strm[1:12], 2)
            strm = strm[12:]
            # Collect packet data
            for n in range(0, num_sub_packets):
                data, strm = read_packets(strm)
                packet_data.append(data)
            return (operate_data(packet_data, type), strm)

# Read packet stream
(result, stream) = read_packets(stream)

# Part 1: report results
print(f"==== PART 1 ====")
print(f"Total versions: {total_versions}\n")

# Part 2: report results
print(f"==== PART 2 ====")
print(f"Result is {result}\n")
