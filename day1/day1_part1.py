# Read input
with open("input.txt") as f:

    # Initialize variables
    num_increases = 0
    curr_depth = int(f.readline())

    # Count number of depth increases in file
    for measurement in f:
        if int(measurement) > curr_depth:
            num_increases += 1
        curr_depth = int(measurement)

# Report number of depth increases
print(num_increases)