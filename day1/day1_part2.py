# Read input
with open("input.txt") as f:

    # Initialize variables
    measurement_window = []
    num_increases = 0

    for measurement in f:
        if len(measurement_window) == 3:
            # Get current sum of measurements in the list, then remove first element
            # and recalculate with the latest measurement instead
            first_sum = sum(measurement_window)
            measurement_window.pop(0)
            second_sum = sum(measurement_window, int(measurement))
            # Count as increase if new sum is bigger
            if second_sum > first_sum:
                num_increases += 1
        # Add new measurement to list
        measurement_window.append(int(measurement))

# Report result
print(num_increases)