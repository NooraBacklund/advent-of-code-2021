import statistics

# Initialize variables
first_illegals = []
autocomplete_scores = []
# Read in data
with open("input.txt") as f:
    # Find first illegal character in each line
    for line in f:
        unresolved_chunk = []
        corrupt = False
        for char in line.strip(): 
            # Push opening characters to unresolved chunk list
            if char in ('(', '[', '{', '<'):
                unresolved_chunk.append(char)
            else:
                last_open = unresolved_chunk[-1]
                # Remove matching character from unresolved chunks
                if (last_open == '(' and char == ')') \
                    or (last_open == '[' and char == ']') \
                    or (last_open == '{' and char == '}') \
                    or (last_open == '<' and char == '>'):
                        unresolved_chunk.pop(-1)
                # Else, illegal character was found
                else:
                    first_illegals.append(char)
                    corrupt = True
                    break
        if not corrupt:
            # All characters examined, autocomplete unresolved chunk
            score = 0
            for character in reversed(unresolved_chunk):
                score *= 5
                if character == '(':
                    score += 1
                elif character == '[':
                    score += 2
                elif character == '{':
                    score += 3
                else:
                    score += 4
            autocomplete_scores.append(score)

score = 0
for character in first_illegals:
    if character == ')':
        score += 3
    elif character == ']':
        score += 57
    elif character == '}':
        score += 1197
    else:
        score += 25137

print(f"==== PART 1 ====")
print(f"Score: {score}\n")

print(f"==== PART 2 ====")
print(f"Autocomplete score: {statistics.median(sorted(autocomplete_scores))}")